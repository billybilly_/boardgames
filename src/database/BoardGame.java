/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author bombardierib
 */
public class BoardGame {
    protected int id;
    protected String title;
    protected double price;
    protected String players;
    protected int inStock;

    
    //Constructeur
    public BoardGame(int id, String title, double price, String players, int inStock) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.players = players;
        this.inStock = inStock;
    }

    public BoardGame(String title, double price, String players, int inStock) {
        this.title = title;
        this.price = price;
        this.players = players;
        this.inStock = inStock;
    }

    public int getId() {
        return id;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    
    
    
    
    
}
